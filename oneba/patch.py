# Patching operations

import os
import shutil

from pathlib import Path


def subs_line(line, values):
    """Substitute 'values' into 'line'.

    Inserts variables in values in places where '{}' is in the line. 'values'
    is an ordered list - they are inserted in their order.
    """

    return line.format(*values)


def repl_file_line(readfile, line_num, subs_line,
                   writefile=None, backup=True, keep_tmp=True,
                   backup_dir=None, tmp_dir=None):
    """Copy 'readfile' line by line into 'writefile' and replace a line number
    'line_num' by 'subs_line'.

    If 'readfile' and 'writefile' are the same (default), 'readfile' is copied
    to 'readfile_backup' (only if it doesn't exists already).

    Parameters
    ----------
    readfile : str
        Input file to be read.
    line_num : str
        Number of the line which will be replaced. (First line is 0.)
    subs_line : str
        String to be written to 'writefile' on the 'line_num'-th line.
    writefile : str
        Output file to be written to.
    backup : bool
        If True, makes a '_backup' file
    keep_backup : bool
        If False, delete the '_backup' file (case of 'readfile' = 'writefile').
    backup_dir : str, Path object
        Directory where to put the '_backup' file.

    Returns
    -------
    int : 1 - successful, 0 - unsuccessful
    """

    if not writefile:
        writefile = readfile
    if not isinstance(readfile, Path):
        readfile = Path(readfile).resolve()
    if not isinstance(writefile, Path):
        writefile = Path(writefile).resolve()
    if (not backup_dir) and (backup):
        backup_dir = readfile.parent
    if not tmp_dir:
        tmp_dir = readfile.parent

    # If changing the same file change the readfile name to 'readfile_backup'
    if str(readfile) == str(writefile):
        if backup:
            backupfile = Path(backup_dir).joinpath(readfile.name + "_orig")
            if not backupfile.exists():
                shutil.copyfile(str(readfile), str(backupfile))
                shutil.copymode(str(readfile), str(backupfile))
        tmpfile = Path(tmp_dir).joinpath(readfile.name + "_tmp")
        readfile.rename(tmpfile)
        infile = Path(tmpfile)
    else:
        tmpfile = None
        infile = Path(readfile)

    # Decode escape characters and strip ' and " from the beginning/end
    subs_line = bytes(subs_line, "utf-8").decode("unicode_escape").strip('"\'')

    with open(infile, 'r') as rf, open(writefile, 'w') as wf:
        for i, line in enumerate(rf):
            if i == int(line_num):
                try:
                    wf.write(subs_line)
                except:
                    wf.close()
                    warnings.warn("Unsuccesful write attempt to:" +
                             "File: {}, Line: {}.".format(writefile, line_num))
                    writefile.unlink()
                    if tmpfile:
                        shutil.copyfile(str(tmpfile), str(readfile))
                    return 0
            else:
                wf.write(line)
    if not keep_tmp:
        try:
            tmpfile.unlink()
        except (FileNotFoundError, AttributeError):
            pass
    return 1


def restore_file(file_path, backup_dir=None, tmp_dir=None):
    """Restore a patched file from a backup and remove a temp file.

    Returns:
        [0,0] ... nothing happened
        [1,0] ... backup restored
        [0,1] ... temp file removed
        [1,1] ... both cases
    """

    status = [0,0]

    if not isinstance(file_path, Path):
        file_path = Path(file_path).resolve()
    if not backup_dir:
        backup_dir = file_path.parent
    if not tmp_dir:
        tmp_dir = file_path.parent

    backup_file = backup_dir.joinpath(file_path.name + "_orig")
    tmp_file = tmp_dir.joinpath(file_path.name + "_tmp")

    # Restore backup
    try:
        shutil.copyfile(str(backup_file), str(file_path))
        shutil.copymode(str(backup_file), str(file_path))
        backup_file.unlink()
        status[0] = 1
    except FileNotFoundError:
        pass
    # Remove temp file
    try:
        tmp_file.unlink()
        status[1] = 1
    except FileNotFoundError:
        pass

    return status


class Patch:
    """This class stores a patch infromation from config file and patch results.
    """

    def __init__(self, name, file_path, rep_line_num, orig_line, rep_line,
                 variables):
        self.name         = name
        self.file_path    = file_path
        self.rep_line_num = rep_line_num
        self.orig_line    = orig_line
        self.rep_line     = rep_line
        self.variables    = variables
        self.failed       = []

    # Static variables
    backup_dir = None
    tmp_dir    = None

    def get_subs_line(self, values):
        return subs_line(self.rep_line, values)

    def run(self, values):
        """Replace a line in a file with specific values.

        Returns:
            1 - succesful, 0 - unsuccesful
        """

        subsline = self.get_subs_line(values)
        ret_val = repl_file_line(self.file_path, self.rep_line_num, subsline,
                                 backup_dir=self.backup_dir,
                                 tmp_dir=self.tmp_dir)
        if ret_val == 0:
            self.failed.append(values)
            return 0
        else:
            return 1

    def clean(self):
        return restore_file(self.file_path, self.backup_dir, self.tmp_dir)
