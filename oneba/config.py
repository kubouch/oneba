import os
import re
import io
from configparser import ConfigParser, ExtendedInterpolation
from pathlib import Path

tgt_conf_file = "targets.conf"
DIRS = {}


def set_root_dir(path):
    """Search through parents of a 'path' until tgt_conf_file is found.

    Directory with tgt_conf_file is then set to be a ROOT_DIR global variable.
    ROOT_DIR should be a path poining to the git repo root directory.

    Examples
    --------
    # start the search from the executed module path
    >>> set_root_dir(os.path.abspath(__file__))
    """
    global ROOT_DIR
    if not isinstance(path, Path):
        path = Path(path)
    for parent in path.parents:
        if list(parent.glob(tgt_conf_file)):
            ROOT_DIR = parent.resolve()
            break


def set_dir(dir_name, path, relative_to=None, ensure=False):
    """Create a new directory entry to DIRS.

    The entry is named 'dir_name'.
    'path' is either a relative path to the 'relative_to' or an absolute path.
    In case of absolute path, 'relative_to' does not have to be specified. If
    'relative_to' is specified in case of absolute path, it will be ignored.
    'path' and 'relative_to' can be both str and Path.
    'ensure' will create the directory if it does not exist.
    """

    if not isinstance(path, Path):
        path = Path(path)
    if not isinstance(relative_to, Path):
        relative_to = Path(relative_to)

    if all([path.exists(), not path.is_dir()]):
        raise NotADirectoryError("{} is not a directory".format(path))
    elif path.is_absolute():
        dirpath = path.resolve()
    else:
        if relative_to:
            dirpath = relative_to.joinpath(path).resolve()
        else:
            raise ValueError("{} is not an absolute path but 'relative_to' "
                             "was not specified.".format(path))

    if ensure:
        DIRS[dir_name] = ensure_path(dirpath)
    else:
        DIRS[dir_name] = dirpath


def ensure_path(path, path_is_file=False):
    """Create all directories to 'path' if they don't exist.

    Returns ensured directory path.
    """

    if not isinstance(path, Path):
        path = Path(path)
    if path_is_file:
        path = path.parent
    if not path.exists():
        os.makedirs(path)
    return path


def set_cfgfile():
    """Set path to the config file into a CFG_FILE global variable.
    """

    local_cfg = Path.home().joinpath(".config/oneba") \
                           .joinpath(tgt_conf_file).resolve()
    global CFG_FILE
    if local_cfg.exists():
        CFG_FILE = local_cfg
    else:
        CFG_FILE = ROOT_DIR.joinpath(tgt_conf_file).resolve()


def set_configparsers():
    """Set a global and target-specific config parsers.
    """
    global cp
    global cp_tgt
    cp     = ConfigCommentsParser(interpolation=ExtendedInterpolation())
    cp_tgt = ConfigCommentsParser(interpolation=ExtendedInterpolation())


def init(root_search_path):
    set_root_dir(root_search_path)
    set_cfgfile()
    set_configparsers()


def print_cp(cp):
    """Print all config parser's sections and options.
    """

    for section in cp.sections():
        print("[{}]".format(section))
        for key in cp[section]:
            print("{}:\n'{}'".format(key, cp[section][key]))


class ConfigCommentsParser(ConfigParser):
    """Modified ConfigParser to store comments.

    Does not store inline comments. If a comment block contains empty lines it
    deletes them as well.

    Use as a standard ConfigParser. read() and write() methods were modified to
    store comments. To use
    """

    def read(self, filenames, encoding=None, read_comments=True):
        super(ConfigCommentsParser, self).read(filenames, encoding)
        if read_comments:
            self.read_comments(filenames)

    def write(self, fp, space_around_delimiters=True, write_comments=True):
        if write_comments:
            with io.StringIO() as sio:
                super(ConfigCommentsParser, self).write(sio,
                                                        space_around_delimiters)
                sio.seek(0)
                self.insert_comments(sio, fp)
        else:
            super(ConfigCommentsParser, self).write(sio,
                                                    space_around_delimiters)

    def read_comments(self, cfgfiles):
        """Read comments and save them together with their context.

        'cfgfiles' can be string, pathlike os a Path object (supported by open)
        as well as an iterable of these.
        """

        if isinstance(cfgfiles, (str, os.PathLike)):
            cfgfiles = [cfgfiles]
        self.comments = {}
        for cfgfile in cfgfiles:
            try:
                file_key = str(cfgfile)
                with open(cfgfile, 'r') as rf:
                    comment_block = []
                    for line in rf:
                        if line.startswith(self._comment_prefixes):
                            comment_block.append(line)
                        elif comment_block:
                            if line.strip():
                                dels = "|".join(self._delimiters)
                                param = re.split(dels, line)[0].strip().lower()
                                self.comments[param] = comment_block
                                comment_block = []
                    if comment_block:    # If last block is comments
                        self.comments['*lastline'] = comment_block
            except OSError:
                continue

    def insert_comments(self, src_fp, dst_fp=None):
        """Read 'in_fp' stream and insert comments according to 'self.comments'.
        Output is written to 'out_fp' stream.

        'in_fp' and 'out_fp' can be the same stream (i.e. a file opened in 'r+'
        mode). Also if 'out_fp' is not specified, it defaults to 'in_fp'.
        """

        lines = src_fp.readlines()
        if not dst_fp:
            dst_fp = src_fp
        dst_fp.seek(0)
        for line in lines:
            try:
                for key in self.comments.keys():
                    if line.startswith(key):
                        dst_fp.writelines(self.comments[key])
                        break
            except AttributeError:
                pass
            dst_fp.writelines(line)
        try:
            dst_fp.writelines(self.comments['*lastline'])
        except KeyError:
            pass
        except AttributeError:
            pass
        dst_fp.truncate()
