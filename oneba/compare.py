# Comparing files

import datetime
import itertools

import config


def get_difname(refname, simname):
    """Create a name for a diff file.

    It is constructed as 'diff' (or 'diff_' + common string between 'refname'
    and 'simname' evaluated from backwards. After encountering an uncommon
    character, evaluation stops.
    """

    difname = ''
    for refchar, simchar in zip(reversed(refname), reversed(simname)):
        if refchar == simchar:
            difname += refchar
        else:
            break
    appendix = difname[::-1]  # reversed
    if appendix.startswith('_'):
        return 'diff' + appendix
    else:
        return 'diff_' + appendix


def cmp_files(reffile, simfile):
    """Compare two files line by line and return the lines if different.

    'reffile' and 'simfile' are can be str or pathlib.Path.
    Returns dictionary of tuples: {line_num : (refline, simline), etc.}
    """

    diffs = {}
    with open(reffile, 'r') as rf, open(simfile, 'r') as sf:
        line_num = 1
        for refline, simline in itertools.zip_longest(rf, sf, fillvalue=''):
            if refline != simline:
                diffs[line_num] = (refline, simline)
            line_num += 1
    return diffs


def write_diff(diffile, diffs, titles):
    """Print differing lines, and optionally their difference, into diff file.

    Difference printing not supported yet.
    """

    if diffs:
        config.ensure_path(diffile, path_is_file=True)
        ncols = len(list(diffs.values())[0]) + 1
        if ncols != len(titles):
            raise ValueError("Number of titles is not matching a number of " +
                             "columns.")

        header_format = "{:<7}" + "{:<36}" * (ncols-1) + '\n'
        line_format = "{:<7}" + "{:<36}" * (ncols-1) + '\n'

        with open(diffile, 'w') as wf:
            wf.write(header_format.format(*titles))
            wf.write('-'*79+'\n')
            for line_num in diffs:
                line = [str(line_num).rjust(5)] + \
                       [repr(s) for s in diffs[line_num]]
                wf.write(line_format.format(*line))
            time_format = "%Y-%m-%d %X %Z"
            timestamp = datetime.datetime.now().astimezone()
            wf.write("\nNumber of errors: {}\n".format(len(diffs)))
            wf.write("Created {}.\n".format(timestamp.strftime(time_format)))


def diff_files(l_ref, l_sim, difdir, mode=None):
    [refdir, reffile] = l_ref
    [simdir, simfile] = l_sim
    difname = get_difname(reffile.name, simfile.name)
    diffile = difdir.joinpath(reffile.parent).joinpath(difname)
    diffs = cmp_files(refdir.joinpath(reffile), simdir.joinpath(simfile))
    if mode != "dry":
        write_diff(diffile, diffs, ["line", str(reffile), str(simfile)])

    return (diffs, diffile)
