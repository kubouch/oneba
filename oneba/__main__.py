#!/usr/bin/env python3
"""
First entry point.

Called by 'python oneba [options]' from the oneba root directory.
"""

import sys
import oneba

def main():
    sys.exit(oneba.main())

if __name__ == "__main__":
    main()
