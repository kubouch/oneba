# Main program module
#
# subprocess.run requires Python 3.5

# native modules
import os
import sys
import subprocess
import argparse
import linecache
from collections import OrderedDict
from pathlib import Path
# oneba modules
import config
import compare
from patch import Patch

## DEBUG
from importlib import reload
import pdb
import patch
reload(patch)
del patch
reload(config)
reload(compare)
from patch import Patch


def str2list(string):
    """Converts string to list.

    Supported string formats:
      1. "x..y"    makes a range from x to y (integers only, y included)
      2. "a, b, x" makes a list [a, b, x]

    Option 2. first tries int, then float. If none of them works, then returns
    a list of strings.
    """

    l = []
    if '..' in string:
        nums = [int(x) for x in string.split('..')]
        l = list(range(nums[0], nums[1]+1))
    elif ',' in string:
        try:
            l = [int(x) for x in string.split(',')]
        except ValueError:
            try:
                l = [float(x) for x in string.split(',')]
            except ValueError:
                l = [x.strip() for x in string.split(',')]
    else:
        l = [string]
    return l


def recurse_dict(var_dict, depth, reg_dict={}):
    """Recursively iterate through every combination of dict values.

    'var_dict' is a dictionary of lists of values. Use collections.OrderedDict
    to preserve the order.

    'reg_dict' is used to store the individual values together in one
    dictionary.
    """

    if (depth > 0):
        values = list(reversed(var_dict.items()))[depth-1][1]
        key    = list(reversed(var_dict.items()))[depth-1][0]
        for val in values:
            reg_dict[key] = val
            if depth == 1:
                yield reg_dict
            yield from recurse_dict(var_dict, depth-1)


def sections_str_iter(cp, s=''):
    """Iterate through config parser's sections and output only those starting
    with 's' string.

    Both 's' and 'cp' section names are considered case insensitive
    """

    for section in cp.sections():
        if section.lower().startswith(s.lower()):
            yield section


def changed_vars(variables, prev_vars):
    """Outputs which variables were changed from previous iteration.

    Returns a dictionary.
    """

    changed = {}
    for key in variables.keys():
        if variables[key] != prev_vars[key]:
            changed[key]= variables[key]
    return changed


def resolve_path(file_path):
    if not isinstance(file_path, Path):
        file_path = Path(file_path)
    if file_path.is_absolute():
        return file_path.resolve()
    else:
        return config.TGT_DIR.joinpath(file_path).resolve()


def get_patches(cp):
    patches = []
    for patch_name in sections_str_iter(cp, "Patch_"):
        patch_file      = resolve_path(cp[patch_name]['file_path'])
        search_str      = cp[patch_name]['search_str']
        # Line numbering from 0 here
        line_num        = int(cp[patch_name]['rep_line_num']) - 1
        original_line   = repr(linecache.getline(str(patch_file), line_num+1))\
                              .strip("'")
        if not (search_str in original_line):
            raise ValueError("File {}:\n'{}' can not be found in line {}:\n{}"
                             .format(patch_file, search_str, line_num+1,
                                     original_line))
        line_to_replace = cp[patch_name]['rep_line'] + "\\n"
        patch_vars      = str2list(cp[patch_name]['variables'])
        patches.append(Patch(patch_name, patch_file, line_num, original_line,
                             line_to_replace, patch_vars))
    return patches


def patch_all(patches, variables, prev_vars, prompt, backup_dir, tmp_dir):
    """Perform all patch operations declared in the targets config file.

    All sections with names starting with 'Patch_' are considered as a patch.

    'variables' is a dictionary of variables and their values to be changed
    """

    changed = changed_vars(variables, prev_vars)
    if prompt.verbosity >= 2:
        print("PATCH (Variables: {}, changed: {}):"
                .format(list(variables.values()), list(changed.values())))
    if prompt.depth >= 2:
        prompt.show()
    for patch in patches:
        # If any of changed variables is in patch.variables list
        if any([x in patch.variables for x in changed.keys()]):
            values = [variables[key] for key in patch.variables]
            if prompt.verbosity >= 2:
                print("'{}' on file {}:".format(patch.name, patch.file_path))
            if prompt.verbosity >= 3:
                print("  replace line {}:".format(patch.rep_line_num+1))
                print("  '{}'".format(patch.orig_line.strip('\n')))
                print("  with\n  '{}'".format(patch.get_subs_line(values)))
            if prompt.depth >= 3:
                prompt.show()
            if not prompt.dry:
                patch.run(values)


def run_script(script_path, exec_with, args, prompt):
    """Runs a script from its directory.

    Parameters:
    -----------
    script_path : pathlib.Path
        Script file
    exec_with : str
        What executes the script (i.e. bash). Can contain options separated
        by space.
    args : list
        List of arguments.

    Returns:
    --------
    out : subprocess.CompletedProcess
        Completed process class with captured stdout from the script
    """

    script_path = Path(script_path).resolve()
    scriptdir = script_path.parent
    exec_list = exec_with.split()
    run_args = exec_list + [script_path.name] + args
    run_args = [' '.join(run_args)]
    if prompt.verbosity >= 2:
        print("(cwd = {}):".format(str(scriptdir)))
        print("  $ {}".format(' '.join(run_args).strip()))
    if prompt.depth >= 2:
        prompt.show()
    if prompt.dry:
        return 0
    else:
        out = subprocess.run(run_args, shell=True, stdout=subprocess.PIPE,
                             cwd=scriptdir)
        return out


class Prompt:
    """Simple class for managing how often to ask for pressing Enter to proceed.
    """

    def __init__(self, depth, verbosity, dry, maxdepth=3, maxverbosity=3):
        self.maxdepth = maxdepth
        self.maxverbosity = maxverbosity
        self.mindepth = 0
        self.minverbosity = 0
        self.depth = depth
        self.verbosity = verbosity
        self.dry = dry
        self._help = ("'c'  - skip all prompts and continue\n" +
                      "'p'  - display current prompt depth\n"  +
                      "'v'  - display current verbosity\n"     +
                      "'pl' - prompt less\n"                   +
                      "'pm' - prompt more\n"                   +
                      "'vl' - verbose less\n"                  +
                      "'vm' - verbose more\n"                  +
                      "'q'  - quit the program\n"              +
                      "'h'  - display this")

    def print_help(self):
        print(self._help)

    def print_depth(self):
        print("Prompt depth is now {}.".format(self.depth))

    def print_verbosity(self):
        print("Verbosity is now {}.".format(self.verbosity))

    def show(self, msg="'Enter' to proceed or 'h' to help: "):
        option = input(msg)
        if option == 'c':
            self.depth = 0
        elif option == 'p':
            self.print_depth()
            self.show(msg)
        elif option == 'v':
            self.print_verbosity()
            self.show(msg)
        elif option == 'pl':
            if self.depth > self.mindepth:
                self.depth -= 1
            self.print_depth()
            self.show(msg)
        elif option == 'pm':
            if self.depth < self.maxdepth:
                self.depth += 1
            self.print_depth()
            self.show(msg)
        elif option == 'vl':
            if self.verbosity > self.minverbosity:
                self.verbosity -= 1
            self.print_verbosity()
            self.show(msg)
        elif option == 'vm':
            if self.verbosity < self.maxverbosity:
                self.verbosity += 1
            self.print_verbosity()
            self.show(msg)
        elif option == 'h':
            self.print_help()
            self.show(msg)
        elif option == 'q':
            sys.exit("Terminated by a user.")
        elif option != '':
            self.show("Invalid option ('h' to help): ")


def ref_sim_files(refdir, simdir, prefix_ref, prefix_sim):
    """Iterate over all files in refdir and simdir.

    Returns a tuple (reffile, simfile) as pathlib.Paths, relative to ref/simdir.
    """

    for (dirpath, dirs, filenames) in os.walk(refdir):
        dirs.sort()
        if filenames:
            filenames.sort()
            for filename_ref in filenames:
                reffile = Path(dirpath).joinpath(filename_ref)
                reffile = reffile.relative_to(refdir)
                filename_sim = prefix_sim + filename_ref[len(prefix_ref):]
                simfile = Path(dirpath).joinpath(filename_sim)
                simfile = simfile.relative_to(refdir)
                if simdir.joinpath(simfile).exists():
                    yield (reffile, simfile, True)
                else:
                    yield (reffile, simfile, False)


def try_rm_dir(dirpath):
    """Try to remove directory at 'dirpath'

    Returns:
        1 - successful (dir removed), 0 - unsuccessful (dir not removed)
    """

    if not isinstance(dirpath, Path):
        dirpath = Path(dirpath)

    try:
        dirpath.rmdir()
        return 1
    except OSError:
        return 0
    except AttributeError:
        return 0


def main():
    """Main program.

    Returns:
        1 - succesful, 0 - unsuccesful
    """

    # Config
    config.init(os.path.abspath(__file__))
    config.cp.read(config.CFG_FILE)

    # Arguments parsing
    tgt_choices = list(config.cp['Targets'].keys())
    tgt_default = tgt_choices[0]
    only_choices = ['patch', 'com', 'sim', 'diff']
    parser = argparse.ArgumentParser(
               description=__doc__)
    parser.add_argument('target', nargs='?', type=str, choices=tgt_choices,
                        help="Specify your target. Choose from options listed"+
                             " in your {} file. (default: {})"
                          .format(config.tgt_conf_file, tgt_default))
    parser.add_argument('-p', '--prompt-depth', type=int, nargs='?',
                        choices=[0, 1, 2, 3], default=1,
                        help="How often you'll be prompted to proceed. " +
                             "0 means never, higher numbers more often. " +
                             "(default: {})".format(1))
    parser.add_argument('-v', '--verbosity', type=int, nargs='?',
                        choices=[0, 1, 2, 3], default=2,
                        help="How often to print to terminal. " +
                             "0 means never, higher numbers more often. " +
                             "(default: {})".format(2))
    parser.add_argument('-e', '--edit', action='store_true', default=False,
                        help="Edit a conf file of a specified target. If no " +
                             "target specified, it opens the main config " +
                             "file.")
    parser.add_argument('--dry', action='store_true', default=False,
                        help="Testing run without touching any files.")
    parser.add_argument('--only', nargs='*', choices=only_choices,
                        default=only_choices,
                        help="Selectively specify which operations to perform"+
                             ": patch/compile/simulate/compare & diff. " +
                             "(default: {})".format(only_choices))
    parser.add_argument('--clean', action='store_true', default=False,
                        help="Restore original files and remove temp files. " +
                              "Overrides '--dry' argument.")
    parser.add_argument('--keep-diffs', action='store_true', default=False,
                        help="Keep diff files from previous runs even if " +
                             "there are no errors.")
    parser.add_argument('--conf', action='store_true', default=False,
                        help="Display locations of all conf files specified.")
    args = parser.parse_args()

    # Resolve target paths
    targets = dict(config.cp['Targets'])
    for (tgt, tgt_file) in targets.items():
        targets[tgt] = Path(targets[tgt])
        if not targets[tgt].is_absolute():
            targets[tgt] = config.CFG_FILE.parent.joinpath(tgt_file).resolve()

    # Edit config file option
    if args.edit:
        editor_cmd = config.cp['Options']['editor']
        if not args.target:
            file_to_edit = config.CFG_FILE
        else:
            file_to_edit = targets[args.target]
        subprocess.run([editor_cmd, file_to_edit])
        return 1

    # Set default target
    if not args.target:
        args.target = tgt_default

    # Config files display
    if args.conf:
        print(args.target)
        print("Main config file:")
        print("{}".format(config.CFG_FILE))
        print("Targets:")
        align_width = 0
        for tgt in targets.keys():
            if len(tgt) > align_width:
                align_width = len(tgt)
        for (tgt, tgt_file) in targets.items():
            print("{}: {} (exists: {})".format(tgt.ljust(align_width + 1),
                                               tgt_file,
                                               tgt_file.exists()))
        return 1

    # Target setup
    config.cp_tgt.read(targets[args.target])
    config.TGT_DIR = targets[args.target].parent
    config.set_dir('ref', config.cp_tgt['Dirs']['reference'], config.TGT_DIR)
    config.set_dir('sim', config.cp_tgt['Dirs']['simulation'], config.TGT_DIR)
    config.set_dir('dif', config.cp_tgt['Dirs']['diff'], config.TGT_DIR)
    config.set_dir('bck', config.cp_tgt['Dirs']['backup'], config.TGT_DIR)
    config.set_dir('tmp', config.cp_tgt['Dirs']['temp'], config.TGT_DIR)

    prompt = Prompt(args.prompt_depth, args.verbosity, args.dry,
                    maxdepth=3, maxverbosity=3)

    file_prefixes = {'ref': config.cp_tgt['Prefixes']['ref_files'],
                     'sim': config.cp_tgt['Prefixes']['sim_files'],
                     'dif': config.cp_tgt['Prefixes']['diff_files']}

    var_dict = OrderedDict()
    prev_vars = OrderedDict()
    for key in config.cp_tgt['Variables']:
        var_dict[key] = str2list(config.cp_tgt['Variables'][key])
        prev_vars[key] = None

    # Dry run info
    if prompt.dry and not args.clean:
        print("*** Dry run ***\n")

    # Target file info
    if prompt.verbosity >= 1:
        print("Reading {}".format(str(config.CFG_FILE)))
        print("Target: {}".format(str(targets[args.target])))

    # Define patch backup and temp directories
    Patch.backup_dir = config.DIRS['bck']
    Patch.tmp_dir    = config.DIRS['tmp']
    # Search for patches
    patches = get_patches(config.cp_tgt)
    if prompt.verbosity >= 1:
        maxlen = len(max([p.name for p in patches], key=len))
        print("\nFound {} patches:".format(len(patches)))
        if prompt.verbosity >= 2:
            for patch in patches:
                print("- {} : {}".format(patch.name.ljust(maxlen),
                                         patch.file_path.name))
        if args.clean:
            print("\nAbout to clean temp and backup files.\n")
        elif args.only != ['diff']:
            print("\nAbout to patch, compile & simulate.")
    if prompt.depth >= 1:
        prompt.show()

    # Clean
    if args.clean:
        # Restore backup files and delete temp files
        prev_file = None
        for patch in patches:
            if prompt.depth >= 2:
                prompt.show()
            status = patch.clean()
            if prompt.verbosity >= 2:
                if prev_file != patch.file_path:
                    print(patch.file_path)
                    if status == [0,0]:
                        print("  nothing to be done")
                    if status[0] == 1:
                        print("  restored backup")
                    if status[1] == 1:
                        print("  removed temp file")
            prev_file = patch.file_path
        # Remove empty directories
        if prompt.verbosity >= 2:
            print('')
        dirs_to_rm = [Patch.backup_dir, Patch.tmp_dir, config.DIRS['dif']]
        for d in dirs_to_rm:
            if all([try_rm_dir(d), prompt.verbosity >=2]):
                print("Removed empty {}.".format(d))
        # Execute clean script
        if prompt.verbosity >= 2:
            print("\nCLEAN ", end='')
        clean_script = resolve_path(config.cp_tgt['Clean_script']['file_path'])
        clean_exec   = config.cp_tgt['Clean_script']['exec_with']
        clean_args   = str2list(config.cp_tgt['Clean_script']['args'])
        run_script(clean_script, clean_exec, clean_args, prompt)
        # End the program
        if prompt.verbosity >= 1:
            print("\nEnding OneBa.")
        return 1


    # Patch/compile/simulate loop
    for variables in recurse_dict(var_dict, len(var_dict)):
        # Patch files
        if 'patch' in args.only:
            # Create 'tmp' and 'bck' directories if they don't exist
            config.ensure_path(Patch.backup_dir)
            config.ensure_path(Patch.tmp_dir)
            if prompt.verbosity >= 2:
                print('')
            patch_all(patches, variables, prev_vars, prompt,
                      config.DIRS['bck'], config.DIRS['tmp'])
            prev_vars = dict(variables)

        # Compile
        if 'com' in args.only:
            if prompt.verbosity >= 2:
                print("COMPILE ", end='')
            com_script = resolve_path(config.cp_tgt['Com_script']['file_path'])
            com_exec   = config.cp_tgt['Com_script']['exec_with']
            com_args   = str2list(config.cp_tgt['Com_script']['args'])
            run_script(com_script, com_exec, com_args, prompt)

        # Simulate
        if 'sim' in args.only:
            if prompt.verbosity >= 2:
                print("SIMULATE ", end='')
            sim_script = resolve_path(config.cp_tgt['Sim_script']['file_path'])
            sim_exec   = config.cp_tgt['Sim_script']['exec_with']
            sim_args   = str2list(config.cp_tgt['Sim_script']['args'])
            run_script(sim_script, sim_exec, sim_args, prompt)

    # Compare loop
    # Iterates reference and simulation output files in an alphabetic order
    if 'diff' in args.only:
        # Create 'diff' directoriy if it doesn't exist
        config.ensure_path(config.DIRS['dif'])
        mode = None
        if prompt.dry:
            mode = "dry"
        if prompt.verbosity >= 1:
            print("\nAbout to compare and create diff files.")
        if prompt.depth >= 1:
            prompt.show()
        diffs = {}
        ndiffs_total = 0
        not_found = []
        diffs_to_rm = []
        for reffile, simfile, exists in ref_sim_files(config.DIRS['ref'],
                                                      config.DIRS['sim'],
                                                      file_prefixes['ref'],
                                                      file_prefixes['sim']):
            if exists:
                diffs, diffile = compare.diff_files(
                                           [config.DIRS['ref'], reffile],
                                           [config.DIRS['sim'], simfile],
                                           config.DIRS['dif'], mode)
                ndiffs = len(diffs)
                ndiffs_total += ndiffs
                if ndiffs:
                    if prompt.verbosity >= 2:
                        if ndiffs == 1:
                            err = "error"
                        else:
                            err = "errors"
                        print("\n{} {} ('{}' vs '{}')".format(ndiffs,
                                                              err,
                                                              reffile,
                                                              simfile))
                        if prompt.verbosity >= 3:
                            for line in diffs:
                                print("Line {}".format(line))
                                print("  ref: {}".format(repr(diffs[line][0])))
                                print("  sim: {}".format(repr(diffs[line][1])))
                        print("Diff file saved to:\n{}".format(str(diffile)))
                    if prompt.depth >= 2:
                        prompt.show()
                elif not args.keep_diffs:
                    if diffile.exists():
                        diffs_to_rm.append(diffile)
            else:
                not_found.append(simfile)
        # Remove empty diff files
        if all([diffs_to_rm, prompt.verbosity >= 1]):
            if len(diffs_to_rm) == 1:
                files_str = "file"
            else:
                files_str = "files"
            print("\nRemoved previous diff {}:".format(files_str))
        for diffile in diffs_to_rm:
            diffile.unlink()
            if prompt.verbosity >= 2:
                print("{}".format(diffile))
        # Print total num. of errors and not found files
        if prompt.verbosity >= 1:
            print("\nTotal number of errors: {}".format(ndiffs_total))
            if not_found:
                if len(not_found) == 1:
                    file_str = "file"
                else:
                    file_str = "files"
                print("\n{} {} not found ({}):"
                        .format(len(not_found), file_str, config.DIRS['sim']))
                if prompt.verbosity >= 2:
                    for f in not_found:
                        print(f)

        # Delete empty diff directory
        if all([try_rm_dir(config.DIRS['dif']), prompt.verbosity >= 2]):
            print("\nRemoved empty {}.".format(config.DIRS['dif']))

    if prompt.verbosity >= 1:
        print("\nEnding OneBa.")

    return 1

if __name__ == "__main__":
    main()
