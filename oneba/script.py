

class Script:
    """This class collects information about a script from a config file.

    Collected info: script name, file path, arguments, execute with

    Might include stdout, stderr, etc.
    """

    def __init__(self, name, file_path, exec_with, args):
        self.name      = name
        self.file_path = file_path
        self.exec_with = exec_with
        self.args      = args
