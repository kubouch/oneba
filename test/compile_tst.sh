#!/bin/sh
# Testing compilation script

if [ "$1" = "clean" ]; then
    rm *.out
else
    OUTNAME=tst.out
    gcc -o $OUTNAME src/main.c $1
fi
