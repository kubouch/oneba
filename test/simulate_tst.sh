#!/bin/sh

# Simulation testing file

SIMDIR=sim

mkdir -p $SIMDIR

TSTFILE="tst.out"
SIMFILE="sim.txt"

./$TSTFILE > "$SIMDIR/$SIMFILE"
