/* This is a dummy source file for testing reftest */

#include <stdio.h>

int main(void)
{
        char str[] = "Hello test!";
        int base = 10;
        int repeats = 5;
        int i;

        printf("%s\n", str);
        for (i = 0; i < repeats; ++i) {
                printf("i:  %d\n", i);
        }

        return 0;
}
